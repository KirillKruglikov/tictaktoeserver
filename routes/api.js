var express = require('express');
var router = express.Router();

var md5 = require('js-md5');

function randInt(min, max) { return Math.round(min - 0.5 + Math.random() * (max - min + 1)); }
function time(){ return new Date().getTime();  }

var players = {};
var games = [];

function createGame(playerToken){
    return {
        players: [playerToken, null],
        field: [[0,0,0],[0,0,0],[0,0,0]],
        currentPlayer: 0,
        win: null,
        status: "pending"
    };
}

function addToGame(playerToken){
    games[games.length - 1].players[1] = playerToken;
    games[games.length - 1].status = "active";
    return games.length - 1;
}



function startGame(){
    var token = md5(time() + "" + randInt(0,9999999));

    var gameIndex = null;

    if (games.length == 0 || games[games.length - 1].players[1] != null){
        gameIndex = ( games.push(createGame(token)) - 1 );
    }else{
        gameIndex = addToGame(token);
    }

    players[token] = gameIndex;

    return {token: token, game: games[gameIndex]};
}

function getGame(token){
    if(typeof  players[token] == "undefined"){
        return startGame();
    }
    return {token: token, game: games[players[token]]};
}

/* GET users listing. */
router.get('/start-game', function(req, res, next) {
    res.json(startGame());
});

router.get('/status', function(req, res, next) {
    res.json(getGame(req.param('token')));
});

module.exports = router;
