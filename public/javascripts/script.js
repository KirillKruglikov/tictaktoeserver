// 0 - ""
// 1 - "X"
// 2 - "O"
var x = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
];
var player = 1;

function render(){
    $("#GameArea .table tr").each(function(i){
        var $tr = $(this);
        $tr.find("td").each(function(j){
            var $td = $(this);
            var $button = $td.find("button");
            switch(x[i][j]){
                case 0:
                    $button.text("");
                    break;
                case 1:
                    $button.text("X");
                    break;
                case 2:
                    $button.text("O");
                    break;
            }
        });
    });


    $("#GameArea").show();
}

render();

function buttonClick(i, j){
    if (x[i][j] != 0){
        return false;
    }

    x[i][j] = player;

    if(player == 1){
        player = 2;
    }else{
        player = 1;
    }
    render();

    var win = checkWin();

    if(win){
        switch(win  ){
            case 1:
                alert("Win X");
                break;
            case 2:
                alert("Win O");
                break;
            case 3:
                alert("Winner not found! Restart game!");
                break;
        }

        allClear();
    }
}

function checkWin(){

    var n = 3;

    for (var i = 0; x.length > i; i++ ){
        for (var j = 0; x[i].length > j; j++ ){
            if(x[i][j] == 0){
                n = 0;
            }
            var win = checkCeil(i, j);
            if(checkCeil(i, j)){
                return win;
            }
        }
    }

    return n;


}

function checkCeil(i, j){
    if( i != 1 && j != 1 ){
        return 0;
    }

    for(var k = 0; k < 2; k++){
        try{
            if( (x[i-1][j-k] == x[i+1][j+k]) && x[i+1][j+k] == x[i][j] ){
                return  x[i][j];
            }
        }catch (e){}

        try{
            if( (x[i+k][j-1] == x[i-k][j+1]) && x[i+k][j-1] == x[i][j] ){
                return  x[i][j];
            }
        }catch (e){}
    }
    return 0;
}

function allClear(){
    for (var i = 0; x.length > i; i++ ){
        for (var j = 0; x[i].length > j; j++ ){
            x[i][j] = 0;
        }
    }
    player = 1;
    render();
}