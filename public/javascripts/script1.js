/* Created by Дмитрий on 17.03.2018. */
$(document).ready(function() {

    var interval = null;


    function render(game){
        $("#GameStatus").text(game.status);
    }

    function refresh(token){
        if(interval != null) {
            clearInterval(interval);
        }
        interval = setInterval(function(){
            $.ajax({
                url: "/api/status?token=" + token,
                dataType: "json"
            })
                .done(function (data) {
                    if(data.token != token){
                        token = data.token;
                        localStorage.setItem("token", data.token);
                    }
                    render(data.game);
                    console.log(data);
                });
        }, 3000);
    }

    function startGame(callback){
        $.ajax({
            url: "/api/start-game",
            dataType: "json"
        })
            .done(function (data) {
                localStorage.setItem("token", data.token);
                token = data.token;
                render(data.game);
                callback(token);
            });
    }

    var token = localStorage.getItem("token");
    if(!token){
       startGame(refresh);
    }else{
        refresh(token);
    }

    $("#newGameButton").on("click", function(){
        startGame(refresh);
    });




});